-- Imports
import XMonad
import qualified XMonad.StackSet as W
import System.Exit

-- Utils
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce

-- Layouts
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

-- Actions
import XMonad.Actions.CopyWindow

-- Programs
myBrowser = "firefox"
myTerminal = "alacritty"
myEditor = "emacsclient -c"
myVideoPlayer = "mpv"
myPdfViewer = "zathura"

-- Gaps 
genGaps i = spacingRaw False (Border i i i i) True (Border i i i i) True 

-- Layouts
myLayouts = tall where 
   tall = renamed [Replace "tall"] $ avoidStruts $ genGaps 10 $ Tall 1 (3/100) (1/2)
_myLayouts = smartBorders $ toggleLayouts Full $ myLayouts

-- Window Rules
myWindowRules = composeAll
    [ className =? "Gimp" --> doFloat ]

-- Workspaces
myWorkspaces = ["home", "term", "browser", "edit", "code", "music", "video", "school", "sys", "home"]
genNumberedWs a b = a ++ " [" ++ b ++ "]"

myNumberedWorkspaces = zipWith (genNumberedWs) myWorkspaces (map (show) [1..(length myWorkspaces)])


-- Keys
myModMask = mod4Mask
myKeys :: [(String, X ())]
myKeys =
    [
      -- Spawn Programs
      ("M-<Return>", spawn (myTerminal)) -- Spawn Terminal (Alacritty)
    , ("M-d", sequence_ [spawn "ffplay -nodisp -autoexit ~/.sounds/dmenu.mp3", spawn "dmenu_run"]) -- Spawn Dmenu
    , ("M-b", spawn (myBrowser)) -- Spawn Browser
    , ("M-e e", spawn "emacsclient -c") -- Spawn Emacs
    , ("M-s i", spawn "dm_impf") -- Spawn my impf dmenu script
    , ("M-w", kill) -- Close a window
    , ("M-f", sendMessage (Toggle "Full")) -- Toggle "fullscreen"
    , ("M-j", windows W.focusDown) -- Focus next window
    , ("M-k", windows W.focusUp) -- Focus last window
    , ("M-m", windows W.focusMaster) -- Focus the master window
    , ("M-S-<Return>", windows W.swapMaster) -- Swap focused window with master
    , ("M-S-j", windows W.swapDown) -- Move a window down
    , ("M-S-k", windows W.swapUp)  -- Move a window up
    , ("M-S-c", windows copyToAll)  -- Move a window up
    , ("M-S-d", killAllOtherCopies )  -- Move a window up
    , ("M-h", sendMessage Shrink) -- Shrink master
    , ("M-l", sendMessage Expand) -- Expand master
    , ("M-t", withFocused $ windows . W.sink) -- Make a window tile
    , ("M-i", sendMessage (IncMasterN 1)) -- Increase windows in the master area
    , ("M-S-d", sendMessage (IncMasterN (-1))) -- Decrease windows in the master area
    -- Layouts
    , ("M-<Tab>", sendMessage NextLayout) -- Next layout
    -- Bar
    , ("M-S-b", sendMessage ToggleStruts) -- Toggle Bar
    -- Xmonad
    , ("M-S-q", io (exitWith ExitSuccess)) -- Quit xmonad
    , ("M-q", spawn "xmonad --recompile; xmonad --restart; killall xmobar")]

-- Startup
myXmobarConfig = def {
   ppCurrent = xmobarColor "#98c379" ""
}
myStartup = do
   spawnOnce "mpv --no-video ~/.sounds/start.mp3 &"
   spawn "nitrogen --restore"
   spawn "setxkbmap -option caps:swapescape"
   spawn "picom --experimental-backends &"
-- The Main Config
main = do
        myXmobar <- statusBarPipe "xmobar ~/.config/xmobar/*" (pure myXmobarConfig)

   	xmonad $ docks $ withSB myXmobar (def {
	       focusedBorderColor = "#61AFEF"
	     , terminal = myBrowser
	     , layoutHook = _myLayouts
	     , startupHook = myStartup
	     , manageHook = myWindowRules
	    -- , handleEventHook =
	     , workspaces = myNumberedWorkspaces
	     , modMask = myModMask
	     -- , logHook =
	} `additionalKeysP` myKeys)

