{-#OPTIONS_GHC -Wno-deprecations #-}
--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

-- Imports

import XMonad
import Data.Monoid
import System.Exit
import Data.Maybe (fromJust)
import System.IO (hPutStrLn)
-- Hooks
import XMonad.Hooks.ManageDocks (ToggleStruts(..), avoidStruts, docksEventHook, manageDocks) -- Used for having windows avoid overlaping the bar and to manage the bar/dock.
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, xmobarBorder,  shorten, PP(..))
import XMonad.Prompt as XP
import XMonad.Actions.DynamicWorkspaces (appendWorkspace)
-- Layouts
import XMonad.Layout.ToggleLayouts as T (toggleLayouts,ToggleLayout(Toggle)) -- Used for toggling layouts
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders (noBorders, smartBorders) -- Layout modifier to remove borders.
import XMonad.Layout.Renamed -- Used for "custom" layouts
import XMonad.Layout.Magnifier
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows
import XMonad.Layout.Maximize (maximize)
import XMonad.Layout.Tabbed 
-- Utils
import XMonad.Util.Run (spawnPipe) -- Used for spawning and running things.
import XMonad.Util.SpawnOnce
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import XMonad.Util.EZConfig(additionalKeysP, additionalMouseBindings)
-- Prompts

-- Actions
import XMonad.Actions.GridSelect

import qualified XMonad.StackSet as T


-- Web Browser
myBrowser= "firefox"

-- Terminal Emulator
myTerminal      = "alacritty"


myGridSelectConf = (buildDefaultGSConfig defaultColorizer) {
	gs_cellheight = 30,
	gs_font = "xft:Roboto Condensed",
	gs_cellwidth = 180
}
	
	
tabConf = def { fontName            = "xft:Roboto Condensed:size=11"
              , activeColor         = "#1b1e24"
              , activeBorderColor   = "#61AFEF"
              , activeTextColor     = "#E06C75" , activeBorderWidth   = 0
              , inactiveColor       = "#282c34"
              , inactiveBorderColor = "#3B4252"
              , inactiveTextColor   = "#E06C75"
              , inactiveBorderWidth = 0
              , urgentColor         = "#282c34"
              , urgentBorderColor   = "#BF616A"
              , urgentBorderWidth   = 0
              }

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Window Border size in pixels
myBorderWidth   = 1

-- Mod key (Mod1 = Alt, Mod4 = Win/Super)
myModMask       = mod4Mask

-- Workspace names
myWorkspaces    = ["Home","Term","Music","Video","Edit","School","Sys","VM", "Etc"]

-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#46d9ff"
-- Key bindings. Add, modify or remove key bindings here.
--
spawnSelectedFromPairs :: GSConfig String -> [(String, String)] -> X ()
spawnSelectedFromPairs conf lst = gridselect conf lst >>= flip whenJust spawn

-- Grid Select Menus
myGSEditors = [
	 ("Emacs", "emacsclient -c || emacs")
	,("Neovim", "yad --class=GSFilePick --file | xargs -n alacritty -e nvim")
	,("Vi", "yad --class=GSFilePick --file | xargs -n alacritty -e vi")
	,("Vim", "yad --class=GSFilePick --file | xargs -n alacritty -e vim")
	,("VSCode", "yad --class=GSFilePick --file | xargs -n vscode")
	,("Less", "yad --class=GSFilePick --file | xargs -n alacritty -e less")
	,("Bat", "yad --class=GSFilePick --file | xargs -n alacritty -e bat")
	,("Emacs No-Gui", "yad --class=GSFilePick --file | xargs -n alacritty emacsclient -nw")]

myKeys :: [(String, X ())]
myKeys = 
    -- Keybindings
    -- Spawn programs
    [ 
      ("M-<Return>", spawn (myTerminal)) -- Spawn Terminal (Alacritty)
    , ("M-d", spawn "dmenu_run") -- Spawn Dmenu
    , ("M-b", spawn (myBrowser))
    , ("M-e e", spawn "emacsclient -c")
    , ("M-s i", spawn "dm_impf")
    , ("M-g e", spawnSelectedFromPairs myGridSelectConf myGSEditors)
    , ("M-w", kill) -- Close a window
    , ("M-f", sendMessage (T.Toggle "nbfull") >> sendMessage ToggleStruts ) -- Toggle "fullscreen"
    , ("M-j", windows W.focusDown) -- Focus next window
    , ("M-k", windows W.focusUp) -- Focus last window
    , ("M-m", windows W.focusMaster) -- Focus the master window
    , ("M-S-<Return>", windows W.swapMaster) -- Swap focused window with master
    , ("M-S-j", windows W.swapDown) -- Move a window down
    , ("M-S-k", windows W.swapUp)  -- Move a window up
    , ("M-h", sendMessage Shrink) -- Shrink master
    , ("M-l", sendMessage Expand) -- Expand master
    , ("M-t", withFocused $ windows . W.sink) -- Make a window tile
    , ("M-i", sendMessage (IncMasterN 1)) -- Increase windows in the master area
    , ("M-S-d", sendMessage (IncMasterN (-1))) -- Decrease windows in the master area
    -- Layouts 
    , ("M-<Tab>", sendMessage NextLayout) -- Next layout
    -- Bar
    , ("M-S-b", sendMessage ToggleStruts) -- Toggle Bar
    -- Xmonad
    , ("M-S-q", io (exitWith ExitSuccess)) -- Quit xmonad
    , ("M-q", spawn "xmonad --recompile; xmonad --restart; killall xmobar")]

-- Layouts
myLayout = avoidStruts $ T.toggleLayouts nbfull $ layouts 
layouts = tall ||| tabs

tabs = renamed [Replace "tabs"]
	$ noBorders
	$ tabbed shrinkText tabConf
nbfull  = renamed [Replace "nbfull"]	
	$ noBorders
	$ Full


tall = renamed [Replace "tall"]
	$ smartBorders
	$ mySpacing 6
	$ Tall 1 (3/100) (1/2)


myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , className =? "Zathura"	    --> doCenterFloat
    , className =? "GSFilePick"     --> doCenterFloat
    , className =? "Pulseterm"	    --> doCenterFloat
    , resource =? "walsel"	    --> doCenterFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore 
    , className =? "JACK_POWERCTL_WIN_CLASS" --> doCenterFloat
    ]

-- Startup programs
myStartUpPrograms :: X ()
myStartUpPrograms = do
        spawn "killall trayer"
	spawn "killall volumeicon"

	spawn "sleep 2 && volumeicon"
	spawnOnce "nitrogen --restore &"	
	spawn "setxkbmap -option caps:swapescape"
	spawnOnce "/usr/libexec/xfce-polkit"
	spawnOnce "picom  --experimental-backends"

	spawn "sleep 2 && trayer --transparent true --alpha 0 --tint 0x282c34 --edge top --align right --width 20 --height 26 --expand true --widthtype request --SetPartialStrut true --SetDockType true"
		
main :: IO ()
main = do 
  xmbar <- spawnPipe "xmobar ~/.config/xmobar/xmobarrc0"

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
  xmonad $ def {
        -- simple stuff
            terminal           = myTerminal,
      focusFollowsMouse  = myFocusFollowsMouse,
      clickJustFocuses   = myClickJustFocuses,
      borderWidth        = myBorderWidth,
      modMask            = myModMask,
      workspaces         = myWorkspaces,
      normalBorderColor  = myNormalBorderColor,
      focusedBorderColor = myFocusedBorderColor,

    -- hooks, layouts
      layoutHook         = myLayout,
      manageHook         = myManageHook <+> manageDocks,
      handleEventHook    = docksEventHook,
      startupHook        = myStartUpPrograms,
      logHook = dynamicLogWithPP $ xmobarPP 
      { ppOutput = \x -> hPutStrLn xmbar x
      		
       , ppCurrent = xmobarColor "#98C379" "" . wrap " " " "
       , ppHiddenNoWindows = xmobarColor "#E06C75" ""  
       , ppHidden =  xmobarColor "#E5C07B" ""
       , ppTitle = xmobarColor "#61AFEF" ""
      ,  ppSep = "<fc=#9e9b99> <fn=4>|</fn> </fc>"

	, ppWsSep = " - "
       , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]                    -- order of things in xmobar
              }
     } `additionalKeysP` myKeys

