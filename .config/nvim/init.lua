local o = vim.o
local opt = vim.opt

opt.expandtab = true
opt.tabstop = 3
opt.shiftwidth= 3
opt.swapfile = false

o.termguicolors = true
o.cmdheight = 1

local function map(m, k, v)
   vim.keymap.set(m, k, v, {silent = true})
end

--
-- Keybindings
--
map("n", "<C-h", "<C-w>h")
map("n", "<C-j", "<C-w>j")
map("n", "<C-k", "<C-w>k")
map("n", "<C-l", "<C-w>l")

map("n", "<C-Right", ":vertical resize +3")
map("n", "<C-Left", ":vertical resize -3")
map("n", "<C-Down", ":resize +3")
map("n", "<C-Up>", ":resize -3")

return require('packer').startup(function()
	use 'tanvirtin/monokai.nvim'
	require('monokai').setup {}
end)
